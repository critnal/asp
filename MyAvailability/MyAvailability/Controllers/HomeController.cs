﻿using System.Web.Mvc;
using MyAvailability.Data;
using System.Linq;

namespace MyAvailability.Controllers
{
    public class HomeController : PermissionsController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            var o = new object();
            var test = o?.ToString();

            var user = db.Users.Where(u => u.Email == "1@test.com");
            db.SaveChanges();

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}