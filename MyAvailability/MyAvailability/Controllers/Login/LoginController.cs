﻿using MyAvailability.Data.Models;
using MyAvailability.Models.Login;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace MyAvailability.Controllers.Login
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                if (login?.Email != null && login?.Password != null)
                {
                    var user = db.Users.FirstOrDefault(
                        u => u.Email == login.Email && u.Password == login.Password);
                    if (user != null)
                    {
                        loginUser(user.UserId);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return View("Index",login);
        }

        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                if (register.Password == register.PasswordRepeat)
                {
                    User user = null;
                    using (var scope = createTransactionScope())
                    {
                        user = db.Users.FirstOrDefault(u => u.Email == register.Email);
                        if (user == null)
                        {
                            user = new User
                            {
                                Email = register.Email,
                                Password = register.Password
                            };
                            db.Users.Add(user);
                        }

                        var organisation = new Organisation
                        {
                            Name = register.OrganisationName
                        };
                        db.Organisations.Add(organisation);

                        db.SaveChanges();

                        var employee = new Employee
                        {
                            Name = register.Name,
                            UserId = user.UserId,
                            OrganisationId = organisation.OrganisationId
                        };
                        db.Employees.Add(employee);
                        db.SaveChanges();

                        scope.Complete();
                    }

                    if (user != null)
                    {
                        loginUser(user.UserId);
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return View(register);
        }

        protected void loginUser(int userId)
        {
            var token = Guid.NewGuid().ToString();
            var userSession = db.UserSessions
                .FirstOrDefault(us => us.UserId == userId);
            if (userSession != null)
            {
                userSession.Token = token;
                db.UserSessions.Attach(userSession);
                db.Entry(userSession).State = EntityState.Modified;
            }
            else
            {
                db.UserSessions.Add(new UserSession
                {
                    UserId = userId,
                    Token = token
                });
            }

            db.SaveChanges();

            Response.Cookies.Add(new HttpCookie(UserSession.COOKIE_NAME, token));
        }
    }
}