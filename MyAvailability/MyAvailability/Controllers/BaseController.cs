﻿using MyAvailability.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace MyAvailability.Controllers
{
    public class BaseController : Controller
    {
        protected MyAvailabilityContext db = new MyAvailabilityContext();

        protected const string COOKIE = "MyAvailabilityCookie";

        protected TransactionScope createTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
    }
}