﻿using MyAvailability.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MyAvailability.Controllers
{
    public class PermissionsAttribute : ActionFilterAttribute
    {
        public int[] PermissionIds { get; protected set; }

        public PermissionsAttribute(params int[] permissionIds)
        {
            PermissionIds = PermissionIds;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (!HttpContext.Current.Request.Cookies.AllKeys.Contains(UserSession.COOKIE_NAME))
            //{
            //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            //    {
            //        controller = "Login",
            //        action = "Index"
            //    }));
            //}

            //base.OnActionExecuting(filterContext);
        }
    }
}