﻿using MyAvailability.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyAvailability.Controllers
{
    [Permissions]
    public class PermissionsController : BaseController
    {
        public User CurrentUser { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var token = Request.Cookies[UserSession.COOKIE_NAME]?.Value;
            if (token != null)
            {
                CurrentUser = db.Users
                    .Join(db.UserSessions,
                        u => u.UserId,
                        us => us.UserId,
                        (u, us) => new { User = u, UserSession = us })
                    .Where(x => x.UserSession.Token == token)
                    .Select(x => x.User)
                    .FirstOrDefault();
            }

            if (CurrentUser == null)
            {
                filterContext.Result = RedirectToAction("Index", "Login");
            }

            base.OnActionExecuting(filterContext);
        }
    }
}