﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyAvailability.Models.Login
{
    public class RegisterViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MinLength(2)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(2)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(2)]
        [Compare("Password")]
        public string PasswordRepeat { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [MinLength(2)]
        public string OrganisationName { get; set; }
    }
}