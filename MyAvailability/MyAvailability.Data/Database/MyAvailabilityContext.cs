﻿using MyAvailability.Data.Models;
using System.Data.Entity;

namespace MyAvailability.Data
{
    public class MyAvailabilityContext : DbContext
    {
        public MyAvailabilityContext() : base("MyAvailability") { }

        public DbSet<User> Users { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
