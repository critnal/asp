﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAvailability.Data.Models
{
    public class UserSession
    {
        public static readonly string COOKIE_NAME = "MyAvailabilityUserSession";

        public int UserSessionId { get; set; }
        public string Token { get; set; }

        //[ForeignKey("User")]
        public int UserId { get; set; }

        //public User User { get; set; }
    }
}
