﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MyAvailability.Data.Models
{
    [Table("Users")]
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        //public UserSession UserSession { get; set; }
    }
}
