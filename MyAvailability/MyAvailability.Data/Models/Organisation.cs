﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MyAvailability.Data.Models
{
    [Table("Organisations")]
    public class Organisation
    {
        public int OrganisationId { get; set; }
        public string Name { get; set; }
    }
}
