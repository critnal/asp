﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MyAvailability.Data.Models
{
    [Table("Employees")]
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int OrganisationId { get; set; }
    }
}
